__author__ = 'gillesmajor'
from marshmallow import Serializer, fields

class TranslationStringSerializer(Serializer):
    id = fields.Integer()
    stringValue = fields.String()
    fkEntryId = fields.Integer()
    fkLanguageId = fields.Integer()
    language = fields.Method("returnStringLanguage")

    def returnStringLanguage(self, translation):
        return translation.language.languageName

    class Meta:
        fields = ('id', 'stringValue', 'fkEntryId', 'fkLanguageId');