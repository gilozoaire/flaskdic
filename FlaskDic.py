#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, traceback, csv, io

from flask import Flask, render_template
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.bootstrap import Bootstrap
from marshmallow import Serializer, fields

from TranslationSerializer import TranslationStringSerializer


#fixme  dutch and english languages are inverted in the json, fix it

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.config['SECRET KEY'] = 'romanofspoulof'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'data.sqlite')
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
db = SQLAlchemy(app)

bootstrap = Bootstrap(app)


#Model
class Translation(db.Model):
    __tablename__ = "translations"
    id = db.Column(db.Integer, primary_key=True)
    stringValue = db.Column(db.String(64))
    fkEntryId = db.Column(db.Integer, db.ForeignKey('entries.id') )
    fkLanguageId = db.Column(db.Integer, db.ForeignKey('languages.id'))

    def __repr__(self):
       return '<Translation: %r>' % self.stringValue

class Language(db.Model):
    __tablename__ = 'languages'
    id = db.Column(db.Integer, primary_key=True)
    languageName = db.Column(db.String(64))
    translations = db.relationship(Translation, backref = "language")

    def __repr__(self):
        return '<Language: %s' % self.languageName


class Entry(db.Model):
    __tablename__ = 'entries'
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String(64))
    translations = db.relationship(Translation, backref = "entry")

    def __repr__(self):
        return '<Entry: %d>' % self.id


# Serializers
class LanguageSerializer(Serializer):
    id = fields.Integer()
    languageName = fields.String()
    translations = fields.Nested(TranslationStringSerializer, many=True)

    class Meta:
        fields = ('id','languageName')


class EntrySerializer(Serializer):
    id = fields.Integer()
    category = fields.String()
    translations = fields.Nested(TranslationStringSerializer, many=True)

    class Meta:
        fields = ('id', 'category', 'translations')


# cvs parser
class CSVExtractor:

    def __init__(self, fileURI, alchDB, outerSeparator, innerSeparator):
        self.URI = fileURI
        self.db = alchDB
        self.outseparator = outerSeparator
        self.innerSeparator = innerSeparator

    def extractFromCSV(self, category):
        languages = Language.query.all()
        with io.open(self.URI, 'rb') as file:
            try:
                self.fileReader = csv.reader(file)
                #Split rows with the separator
                for row in self.fileReader:
                    newEntry = Entry(category = category)
                    row[0] = row[0].replace('=', '/').decode('utf8')
                    splitRowList = row[0].split(self.outseparator)
                    for columnIndex in range(0, len(splitRowList)):
                        #Construct lists of translations depending on the index
                        # search for synonyms using the inner separator and add them to the translations table
                        #todo :find how to trim leading and trailing white spaces
                        wordList = splitRowList[columnIndex].split(self.innerSeparator)
                        # for each separate word, create a translation that is linked to the current entry and the correct language
                        for i in range(0, len(wordList)):
                            wordList[i] = wordList[i].strip()
                            translation = Translation (language = languages[columnIndex], stringValue = wordList[i], entry = newEntry)
                            db.session.add(translation)
                db.session.add(newEntry)
            except:
                print traceback.print_exc()
            finally:
                file.close()
                db.session.commit()




# app routes
@app.route('/viewentries',methods=['GET'])
@app.route('/', methods=['GET'])
def viewEntries():
    columns = ["French", "Dutch", "English", "Actions"]
    try:
        entries = Entry.query.all()
    except Exception:
        traceback.print_exc()
        traceback.print_exception()
    return render_template("viewentries.html", entries = entries, columns = columns)

def getDb():
    return db

#todo: implement with paging for both methods
@app.route('/entries/')
def entries():
    try:
        #test csv reader inserter
        """
        db.create_all()
        fr = Language(languageName="French")
        du = Language(languageName="Dutch")
        en = Language(languageName="English")
        db.session.add_all([fr, du, en])
        db.session.commit()

        csvExtractor = CSVExtractor("/home/gilozoaire/mysite/dicMed.csv", db,';','/');
        csvExtractor.extractFromCSV("meds");
        """
        entries = Entry.query.all()
        serialized = EntrySerializer(entries, many=True)

    except Exception:
        traceback.print_exc()
        traceback.print_exception()

    return serialized.json


@app.route('/translations/')
def translations():
    translations = Translation.query.all()
    return TranslationStringSerializer(translations, many=True).json

@app.route('/languages/')
def languages():
    langs = Language.query.all()
    return LanguageSerializer(langs, many=True).json

@app.route('/entries/<id>')
def item(id):
    entry = db.session.query.filter(Entry.id == id)
    return EntrySerializer(entry).json

@app.route('/translations/<id>')
def translation(id):
    translation = db.session.query.filter(Translation.id == id)

    return TranslationStringSerializer(translation).json
    """
#Custom error handling
@app.errorhandler(404)
def page_not_found(e):
    return render_template('/errors/404.html'), 404

@app.errorhandler(500)
def internal_server_error(e):
    return render_template('/errors/500.html'), 500
    """

if __name__ == '__main__':
    app.run()



