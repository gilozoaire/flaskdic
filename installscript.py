__author__ = 'gillesmajor'

from FlaskDic import db 

db.create_all();

fr = Language(languageName="French")
en = Language(languageName="English")
du = Language(languageName="Dutch")

db.session.add_all([fr, en, du])
db.session.commit()

